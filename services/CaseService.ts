import * as fs from 'fs';
import * as util from 'util';
//import { PublicExamResult } from '../model/interface';
//import { User } from '../model/interface';
import { Case } from '../model/interface';
const fsReadFilePromise = util.promisify(fs.readFile);
const fsWriteFilePromise = util.promisify(fs.writeFile);

export class CaseService {
   
  async getAllCases() {
    const casesFile = await fsReadFilePromise('./case.json', 'utf8');
    const cases = JSON.parse(casesFile); // convert JSON string to array
    // for (let user of users) {
    //   delete user.password;
    // }

    return cases;
  }

  async createOneCase(newcase:Case) {
        
    const casesFile = await fsReadFilePromise('./case.json', 'utf8');
    const cases = JSON.parse(casesFile);
    const newCase = {
        'applyDate':new Date(),
        'subject':newcase.subject,
        'year':newcase.year,
        'moneyPerHour':newcase.moneyPerHour,
        'location':newcase.location,
        'locationDetail':newcase.locationDetail,
        'lessonPerWeek':newcase.lessonPerWeek,
        'require':newcase.require,
        'tel':newcase.tel,
        'email':newcase.email,
        'status':'open',
        'id': cases.length + 1,


      
    };
    cases.push(newCase);
    await fsWriteFilePromise('./case.json', JSON.stringify(cases));
  }


async changeStatus(id: string,changeObject: any){
    const caseFile = await fsReadFilePromise('./case.json', 'utf8');
            const cases = JSON.parse(caseFile);
            console.log(`ID is ${id}`)
            const onecase = cases.find(function (onecase: any) {
                if (onecase.id == id) {
                    return true;
                } else {
                    return false;
                }
            });
            console.log(cases);
            console.log(onecase);

            if (typeof onecase==='undefined'){
                
                throw new Error('no_user');
            }
            if (changeObject.applyUserIDs){
                changeObject.applyUserIDs.push(onecase.applyUserIDs);
            }
            Object.assign(onecase,changeObject)
            
                await fsWriteFilePromise('./case.json', JSON.stringify(cases));
                return ;
}

async delete(id: string){
    const caseFile = await fsReadFilePromise('./case.json', 'utf8');
            const cases = JSON.parse(caseFile);
            const onecase = cases.find(function (onecase: any) {
                if (onecase.id === id) {
                    return true;
                } else {
                    return false;
                }
            });
            if (typeof onecase==='undefined'){
                
                throw new Error('no_case');
            }

            cases.splice(cases.indexOf(onecase),1);
            await fsWriteFilePromise('./case.json', JSON.stringify(cases));
            return ;
                
}

}
