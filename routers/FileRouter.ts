import * as express from 'express';



import * as multer from 'multer';

import { FileService } from '../services/FileService';

const upload = multer({ dest: '/tmp' })





export class FileRouter {
    private fileService: FileService;
    constructor() {
        this.fileService = new FileService('./uploads');

    }

    router() {
        const router = express.Router();
        router.get('/directories', async (req, res) => {


            res.json(await this.fileService.findDirectories(req.query.folder));

        });


        //GET /files/directories?folder={folder} 
        //GET /files/files?folder={folder}




        router.get('/files', async (req, res) => {


            res.json(await this.fileService.findFiles(req.query.folder));

        });

        //POST /files/directory?path={path}
        router.post('/directory', async (req, res) => {

            await this.fileService.mkdir(req.query.path);
            res.json({ result: 'success' });
        });

        router.get('/file', async (req, res) => {
            try {
                const file = await this.fileService.getfullPath(req.query.path);
                res.sendFile(file);
            } catch (err) {
                res.status(404).json({ result: err.message });
            }

        })

        router.post('/file', upload.single('the_file'), async (req, res) => {

            try {

                this.fileService.createFile(req.file.path, req.query.path);
                res.json({ result: 'success' });
            } catch (err) {
                res.status(400).json({ result: err.message});
            }


        });
        router.put('/file', upload.single('the_file'), async (req, res) => {
            try {

                this.fileService.modifyFile(req.file.path, req.query.path);
                res.json({ result: 'success' });
            } catch (err) {
                res.status(404).json({ result: err.message });
            }

        });

        router.delete('/directory', async (req, res) => {

            try {

                this.fileService.deleteFile(req.query.path);
                res.json({ result: 'success' });
            } catch (err) {
                res.status(404).json({ result: err.message });
            }





        })
        return router;
    }

}





