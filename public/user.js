window.onload = () => {
  console.log('on load');

  fetch('/users/me', {
    method: 'GET',
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },

   
  }).then(res => {
    return res.json();
  
  }).then(res=>{
    document.getElementById("university").value = res.university;
    document.getElementById("year").value = res.year;
    document.getElementById("address").value = res.address;
    document.getElementById("addressDetail").value = res.addressDetail;
    document.getElementById("subject").value = res.subject;
    document.getElementById("introduction").value = res.introduction;
    document.getElementById("tel").value = res.tel;
    document.getElementById("email").value = res.email;
  })


  const createForm = document.querySelector('#create-form') // grab the form 
    .addEventListener('submit', function (event) { // add a listener upon submission
      event.preventDefault();        // ^^^ given by DOM
      // ^^^^ stop the default behaviour (submission in foreground)

      // Serialize the Form afterwards
      // const form = this; // the `this` will be converted to the element (which is form)
      const form = event.currentTarget; // event.currentTarget is also the element triggering the event
      const formData = {}; // object
      for (let input of form) { // treat the form like an array
        if (!['submit', 'reset'].includes(input.type)) {
          formData[input.name] = input.value; // assign value to the object
        }
      }
      console.log(formData);

      fetch('/users/', {
        method: 'POST',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formData)
      }).then(res => {
        return res.json();
      }).then(body => {
        console.log(body);
        if (body.result == 'success') {
          //  vvvv HTML/DOM object given by the browser
          alert('Done');
          fetch('/users/', {
            method: 'GET',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
            },

          }).then(res => {
            return res.json();
          }).then(body => {
            const usersList = document.querySelector('#users-main');
            console.log('the element is', usersList);

            const usersListHTML = UserList(body);
            console.log(usersListHTML);

            usersList.innerHTML = usersListHTML;
            data.users = body;


            console.log(body);
            if (body.result == 'success') {
              //  vvvv HTML/DOM object given by the browser
              alert('WE have below user '); // redirect the browser to other location
              //       ^^^ magic property that when you change it, the browser will jump to that URL
            }
          });
          // redirect the browser to other location
          //       ^^^ magic property that when you change it, the browser will jump to that URL
        } else {
          alert('Wrong username or password :P');
        }
      });
    })


  const changeForm = document.querySelector('#change-form') // grab the form 
    .addEventListener('submit', function (event) { // add a listener upon submission
      event.preventDefault();        // ^^^ given by DOM
      // ^^^^ stop the default behaviour (submission in foreground)

      // Serialize the Form afterwards
      // const form = this; // the `this` will be converted to the element (which is form)
      const form = event.currentTarget; // event.currentTarget is also the element triggering the event
      const formData = {}; // object
      for (let input of form) { // treat the form like an array
        if (!['submit', 'reset'].includes(input.type)) {
          formData[input.name] = input.value; // assign value to the object
        }
      }
      console.log("formdata",formData);

      fetch('/users/' + formData.username, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },

        body: JSON.stringify(formData)
      }).then(res => {
        return res.json();
      }).then(body => {
        console.log(body);
        if (body.result == 'success') {
          //  vvvv HTML/DOM object given by the browser
          alert('Done');
          fetch('/users/', {
            method: 'GET',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
            },

          }).then(res => {
            return res.json();
          }).then(body => {
            const usersList = document.querySelector('#users-main');
            console.log('the element is', usersList);

            const usersListHTML = UserList(body);
            console.log(usersListHTML);

            usersList.innerHTML = usersListHTML;
            data.users = body;


            console.log(body);
            if (body.result == 'success') {
              //  vvvv HTML/DOM object given by the browser
              alert('WE have below user '); // redirect the browser to other location
              //       ^^^ magic property that when you change it, the browser will jump to that URL
            }
          }); // redirect the browser to other location
          //       ^^^ magic property that when you change it, the browser will jump to that URL
        } else {
          alert('Wrong username or password :P');
        }
      });
    })


  const deleteForm = document.querySelector('#delete-form') // grab the form 
    .addEventListener('submit', function (event) { // add a listener upon submission
      event.preventDefault();        // ^^^ given by DOM
      // ^^^^ stop the default behaviour (submission in foreground)

      // Serialize the Form afterwards
      // const form = this; // the `this` will be converted to the element (which is form)
      const form = event.currentTarget; // event.currentTarget is also the element triggering the event
      const formData = {}; // object
      for (let input of form) { // treat the form like an array
        if (!['submit', 'reset'].includes(input.type)) {
          formData[input.name] = input.value; // assign value to the object
        }
      }
      console.log(formData);

      fetch('/users/' + formData.username, {
        method: 'Delete',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formData)
      }).then(res => {
        return res.json();
      }).then(body => {
        console.log(body);
        if (body.result == 'success') {
          //  vvvv HTML/DOM object given by the browser
          alert('Done');
          fetch('/users/', {
            method: 'GET',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
            },

          }).then(res => {
            return res.json();
          }).then(body => {
            const usersList = document.querySelector('#users-main');
            console.log('the element is', usersList);

            const usersListHTML = UserList(body);
            console.log(usersListHTML);

            usersList.innerHTML = usersListHTML;
            data.users = body;


            console.log(body);
            if (body.result == 'success') {
              //  vvvv HTML/DOM object given by the browser
              alert('WE have below user '); // redirect the browser to other location
              //       ^^^ magic property that when you change it, the browser will jump to that URL
            }
          }); // redirect the browser to other location
          //       ^^^ magic property that when you change it, the browser will jump to that URL
        } else {
          alert('Wrong username or password :P');
        }
      });
    })

  const showForm = document.querySelector('#show-form') // grab the form 
    .addEventListener("click", function (event) { // add a listener upon submission
      event.preventDefault();        // ^^^ given by DOM
      // ^^^^ stop the default behaviour (submission in foreground)

      // Serialize the Form afterwards
      // const form = this; // the `this` will be converted to the element (which is form)


      fetch('/users/', {
        method: 'GET',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },

      }).then(res => {
        return res.json();
      }).then(body => {
        const usersList = document.querySelector('#users-main');
        console.log('the element is', usersList);

        const usersListHTML = UserList(body);
        console.log(usersListHTML);

        usersList.innerHTML = usersListHTML;
        data.users = body;


        console.log(body);
        if (body.result == 'success') {
          //  vvvv HTML/DOM object given by the browser
          alert('WE have below user '); // redirect the browser to other location
          //       ^^^ magic property that when you change it, the browser will jump to that URL
        }
      });
    })

};


const data = {
  users: []
}

async function populateData() {
  const res = await fetch('/users/')
  const users = await res.json();
  console.log('WE have below user', users);

  const usersList = document.querySelector('#users-main');
  console.log('the element is', usersList);

  const usersListHTML = UsersList(users);
  console.log(usersListHTML);

  usersList.innerHTML = usersListHTML;
  data.users = users; // <<<<< store fetched data
}

function UserList(users) {
  return `
      <ul id="user-list">
          ${
    users.map((user) => {
      return User(user)
    }).join('')
    }
      </ul>
  `
}

function deleteUser(userName) {
  const user = data.users.find((user) => user.username == userName);
  fetch('/users/' + user.username, {
    method: 'Delete',
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify(user)
  }).then(res => {
    return res.json();
  }).then(body => {
    console.log(body);
    if (body.result == 'success') {
      //  vvvv HTML/DOM object given by the browser
      alert('Done');
      fetch('/users/', {
        method: 'GET',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },

      }).then(res => {
        return res.json();
      }).then(body => {
        const usersList = document.querySelector('#users-main');
        console.log('the element is', usersList);

        const usersListHTML = UserList(body);
        console.log(usersListHTML);

        usersList.innerHTML = usersListHTML;
        data.users = body;


        console.log(body);
        if (body.result == 'success') {
          //  vvvv HTML/DOM object given by the browser
          alert('WE have below user '); // redirect the browser to other location
          //       ^^^ magic property that when you change it, the browser will jump to that URL
        }
      });// redirect the browser to other location
      //       ^^^ magic property that when you change it, the browser will jump to that URL
    } else {
      alert('Wrong username or password :P');
    }

  })
}
function editUser(userName) {
  const user = data.users.find((user) => user.username == userName);
  populateForm(user);
}

function populateForm(user) {
  console.log('populateForm get an object:', user);

  const userForm = document.querySelector('#change-form');

  for (let key of Object.keys(user)) {
    console.log('looping key', key);
    userForm.querySelector(`[name=${key}]`).value = user[key]; // apple[key], apple.id, apple.breed, apple.weight ...
  }
}




function User(user) {

  let information = '<li class="user-item">'

  for (let key of Object.keys(user)) {
    information = information + `
        <div> ${key} : ${user[key]}</div> `
  }

  information = information + '</li>'


  return information
}
