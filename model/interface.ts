export interface PublicExamResult{
    subjectId:number;
    subjectName:string;
    subjectGrade:string;
  }
  export interface User {
    id: number;
    username: string;
    password?: string;
    googleLink?: string;
    university?:string;
    subject?:string;
    publicExamResult?:PublicExamResult;
    year?:number;
    address?:string;
    addressDetail?:string;
    introduction?:string;
    recordHistory?:string;
    resisterDate?:string;
    level?:string;
    tel?:number;
    email?:string;

  }
  export interface Case {
    id: number;
    applyDate?:string;
    subject?:string;
    year?:number;
    moneyPerHour?:number;
    location?:string;
    locationDetail?:string;
    lessonPerWeek?:number;
    require?:string;
    tel?:number;
    email?:string;
    status?:string;
    hoursPerLesson?:string;
    applyUserIDs?:string[];

  }