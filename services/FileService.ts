import * as fs from 'fs';
import * as util from 'util';

import * as path from 'path';
const fsReadDirPromise = util.promisify(fs.readdir);
const fsStatPromise = util.promisify(fs.stat);
const fsMkdirPromise = util.promisify(fs.mkdir);
const fsExistsPromise = util.promisify(fs.exists);
const fsRenamePromise = util.promisify(fs.copyFile);
const fsRmdirPromise = util.promisify(fs.rmdir);


export class FileService{
    constructor(private rootFolder:string){

    }

    private resolveRelativePath(relativePath:string){
        let resolvedPath = this.rootFolder;
            if (typeof relativePath !== 'undefined'){
                resolvedPath=resolvedPath+'/'+relativePath;
            }
            return resolvedPath;
    }
    async findDirectories(relativePath:string){
        let folder = this.resolveRelativePath(relativePath);
        const files = await fsReadDirPromise(folder);
            const result = [];
            for (let file of files) {
                const fullPath = folder+'/'+ file;
                const stat = await fsStatPromise(fullPath);
                if (stat.isDirectory()) {
                    result.push(file);
                }
            }
            return result;

    }
    async findFiles(relativePath:string){
        let folder = this.resolveRelativePath(relativePath);
        const files = await fsReadDirPromise(folder);
            const result = [];
            for (let file of files) {
                const fullPath = folder+'/'+ file;
                const stat = await fsStatPromise(fullPath);
                if (!stat.isDirectory()) {
                    result.push(file);
                }
            }
            return result;
    }
     mkdir(relativePath:string){
        let folder = this.resolveRelativePath(relativePath);
        return fsMkdirPromise(folder);
    }

    getfullPath(relativePath:string){
        let file = this.resolveRelativePath(relativePath);
        if(fsExistsPromise(file)){
            return path.resolve(file);
        }else{
            throw new Error ('not_found');
        }
        
    }

    async createFile(uploadedPath:string,newRelativePath:string){
        let newPath = this.resolveRelativePath(newRelativePath);
        newPath =path.resolve(newPath);
           if (!await fsExistsPromise(newPath)){
               await fsRenamePromise(uploadedPath, newPath);
               return;
           }else{
               throw new Error('file_exits');
           }

    }

    async modifyFile(uploadedPath:string,newRelativePath:string){
        let newPath = this.resolveRelativePath(newRelativePath);
        newPath =path.resolve(newPath);
           if (await fsExistsPromise(newPath)){
               await fsRenamePromise(uploadedPath, newPath);
               return;
           }else{
               throw new Error('file_exits');
           }

    }

    async deleteFile(relativePath:string){
    let newPath = this.resolveRelativePath(relativePath);
    newPath=path.resolve(newPath);
                   if(await fsExistsPromise(newPath)){
                       //await fsUnlinkPromise(newPath);
                       await fsRmdirPromise(newPath);
                       return;
                   }else{
                    throw new Error('file_not exits');
                   }
                }
    
}



