import * as express from 'express';
import * as fs from 'fs';
import * as util from 'util';
import * as expressSession from 'express-session';
import * as bodyParser from 'body-parser';
import { NextFunction } from 'express-serve-static-core';
import { FileRouter} from './routers/FileRouter';
import { UserRouter} from './routers/testUserRouter';
import { CaseRouter} from './routers/CaseRouter';
import * as passport from 'passport'
import * as passportLocal from 'passport-local';
import * as passportOauth2 from 'passport-oauth2';
//import { PublicExamResult } from './model/interface';
import { User } from './model/interface';
//import { Case } from './model/interface';
const fsWriteFilePromise = util.promisify(fs.writeFile);
import axios from 'axios';

const fsReadFilePromise = util.promisify(fs.readFile);

const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
// interface PublicExamResult{
//   subjectId:number;
//   subjectName:string;
//   subjectGrade:string;
// }
// interface User {
//     id: number;
//     username: string;
//     password?: string;
//     googleLink?: string;
//     university?:string;
//     subject?:string;
//     publicExamResult?:PublicExamResult;
//     year?:number;
//     address?:string;
//     addressDetail?:string;
//     introduction?:string;
//     recordHistory?:string;
//     resisterDate?:string;
//     level?:string;
//     tel?:number;
//     email?:string;

//   }
  // interface Case {
  //   applyDate?:string;
  //   subject?:string;
  //   year?:number;
  //   moneyPerHour?:number;
  //   location?:string;
  //   locationDetail?:string;
  //   lessonPerWeek?:number;
  //   require?:string;
  //   tel?:number;
  //   email?:string;

  // }
//   const users:User[] = [
//     {
//       id: 1,
//       username: 'alex',
//       password: '1234',
//       googleLink: '',
//     },
//     {
//       id: 2,
//       username: 'alexattecky',
//       password: '1234',
//       googleLink: 'alex@tecky.io',
//     },
//   ];
  
  const OAuth2Strategy = passportOauth2.Strategy;
const GOOGLE_CLIENT_ID = "909372256416-lc0pdci0a7c166fsh6ma94mv74svkcpc.apps.googleusercontent.com";
const GOOGLE_CLIENT_SECRET = "dI3iqV8Z9nPyxFQiYE0YkKzA"
passport.use('google',new OAuth2Strategy({
    authorizationURL: 'https://accounts.google.com/o/oauth2/auth',
    tokenURL:"https://accounts.google.com/o/oauth2/token",
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:8080/auth/google/callback"
  },
  async function(accessToken:string, refreshToken:string, profile:any, done:Function) {
    const googleResponse = await axios({
        url:'https://www.googleapis.com/oauth2/v2/userinfo',
        method: "get",
        headers: {
            "Authorization":`Bearer ${accessToken}`
        }
    });
    console.log(accessToken);
    const userFile = await fsReadFilePromise('./user.json', 'utf8');
    const users = JSON.parse(userFile);
    let user = users.find((user:User)=>user.googleLink == googleResponse.data.email);
    if(!user){
      // return done(new Error("User not found"));
      const newUser = {
        id: users.length + 1,
        username: googleResponse.data.email,
        googleLink: googleResponse.data.email
      }

      users.push(newUser);
      await fsWriteFilePromise('./user.json', JSON.stringify(users));
      user = newUser;
    }
    done(null,{accessToken,refreshToken,username:user.username})
  }
));

const LocalStrategy = passportLocal.Strategy;

passport.use('local', new LocalStrategy(
  async function(username, password, done) {
    const userFile = await fsReadFilePromise('./user.json', 'utf8');
    const users = JSON.parse(userFile);
    const user = users.find((user:User) => user.username == username);
    if (!user){
      return done(null, false, {message: 'Incorrect username!'});
    }
    const match = (password == user.password);
    if (match) {
      return done(null, user);
    } else {
      return done(null, false, {message:'Incorrect password!'});
    }
  }
));

passport.serializeUser(function(user: User, done) {
  done(null, user.username);
});

passport.deserializeUser(async function(username, done) {
    const userFile = await fsReadFilePromise('./user.json', 'utf8');
    const users = JSON.parse(userFile);
  const user = users.find((user:User)=> username == user.username);
  if(user){
      done(null,user);
  }else{
      done(new Error("User not Found"));
  }
});

app.use(express.static('./public/'));


app.get('/check',async (req,res)=>{
    if(!req.user){
        res.status(401).json({result:'no'});
        return;
    }else{
        res.status(401).json({result:'ok'});
        return;
    }
});


// app.post('/login',async (req,res)=>{
    
//     const userFile = await fsReadFilePromise('./user.json', 'utf8');
//             const users = JSON.parse(userFile);
//             const user = users.find(function (user: any) {
//                 if (user.username === req.body.username&& user.password===req.body.password) {
//                     return true;
//                 } else {
//                     return false;
//                 }
//             });
//             if (typeof user==='undefined'){
//                 res.status(401).json({result:'not authorized'});
//                 return;
//             } else{
//                 if(typeof req.session !== 'undefined'){
//                     req.session.user = req.body.username;
//                     req.session.level= user.level;
//                     res.json({result:'ok'});}
//             }
       

//     if(req.body.username ==='admin'&& req.body.password==='admin'){
//         if(typeof req.session !== 'undefined'){
//         req.session.user = 'admin';
//         res.json({result:'ok'});}
//     else{
//         res.status(500).json({result:'session not found'})
//     }
// }else{
//     res.status(401).json({result:'not authorized'});
// }
    
    
// });



app.get('/auth/google/',passport.authenticate('google',{
    scope:['email','profile']})); // redirect to google
  
  app.get('/auth/google/callback', passport.authenticate('google', {
    failureRedirect:"/login.html"
  }), (req,res)=>{
    res.redirect('/cases.html')
  });
  
  app.post('/login', passport.authenticate('local', {
    failureRedirect: '/login.html'
  }), (req, res) => {
    res.redirect('/case.html');
  })

  app.get('/logout', isLoggedIn, (req, res) => {
    req.logOut();
    res.redirect('/login.html');
  });
  
  app.get('/getcurrentuser', isLoggedIn, (req, res) => {
    
    res.json({user:req.user});
  });
  
  

// function isLevel2(req:express.Request,res:express.Response,next:NextFunction){
//     if( req.user.level!=='admin'){
//         res.status(401).json({result:'not_autorized'});
//         return;
//     }else{
//         next();
//     }
// }

function isLoggedIn(req:express.Request,res:express.Response,next:NextFunction){
    if(req.user){
        next();
      }else{
        res.redirect('/login.html');
      }
    
}
const fileRouter = new FileRouter();
app.use('/files',fileRouter.router());
//app.use('/files',isLoggedIn,fileRouter.router());

const userRouter = new UserRouter();
app.use('/users',userRouter.router());
//app.use('/users',userRouter.router());


const caseRouter = new CaseRouter();
//app.use('/cases',isLoggedIn,isLevel2,caseRouter.router());
app.use('/cases',caseRouter.router());



app.listen(8080);