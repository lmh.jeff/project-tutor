
window.onload = ()=>{
    console.log('on load');




document.querySelector('#list-directories-form') // grab the form 
.addEventListener('submit', function(event) { // add a listener upon submission
    event.preventDefault();        // ^^^ given by DOM
    // ^^^^ stop the default behaviour (submission in foreground)

    // Serialize the Form afterwards
    // const form = this; // the `this` will be converted to the element (which is form)
    const form = event.currentTarget; // event.currentTarget is also the element triggering the event
    const formData = {}; // object
    for(let input of form) { // treat the form like an array
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value; // assign value to the object
        }
    }
    console.log(formData);

    fetch('/files/directories?folder='+formData.Path, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      
     
    }).then(res => {
      return res.json();
    }).then(body => {
      console.log(body);
      alert('WE have below directories '+JSON.stringify(body) );
       const filesList = document.querySelector('#files-main');
  console.log('the element is', filesList);

  const filesListHTML = fileList(body);
  console.log(filesListHTML);

  filesList.innerHTML = filesListHTML;
  data.files = body;
    });
})


document.querySelector('#download-file-form') // grab the form 
.addEventListener('submit', function(event) { // add a listener upon submission
    event.preventDefault();        // ^^^ given by DOM
    // ^^^^ stop the default behaviour (submission in foreground)

    // Serialize the Form afterwards
    // const form = this; // the `this` will be converted to the element (which is form)
    const form = event.currentTarget; // event.currentTarget is also the element triggering the event
    const formData = {}; // object
    for(let input of form) { // treat the form like an array
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value; // assign value to the object
        }
    }
    console.log(formData);
    

    fetch('/files/file?path='+formData.Path).then(res => res.blob().then(blob => {
    var a = document.createElement('a');
    var url = window.URL.createObjectURL(blob);
    var filename = formData.Path;
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
}))

    // fetch('/files/file?path='+formData.Path, {
    //   method: 'GET',
    //   headers: {
    //     "Content-Type": "application/json; charset=utf-8",
    //   },
      
      
    // }).then(res => {
    //   return res.json();
    // }).then(body => {
    //   console.log(body);
    //   if (body.result == 'success') {
    //     //  vvvv HTML/DOM object given by the browser
    //     alert('Done'); // redirect the browser to other location
    //     //       ^^^ magic property that when you change it, the browser will jump to that URL
    //   } else {
    //     alert('Wrong username or password :P');
    //   }
    // });
})


document.querySelector('#upload-form') // grab the form 
.addEventListener('submit', function(event) { // add a listener upon submission
    event.preventDefault();        // ^^^ given by DOM
    // ^^^^ stop the default behaviour (submission in foreground)

    // Serialize the Form afterwards
    // const form = this; // the `this` will be converted to the element (which is form)
    const form = event.currentTarget; // event.currentTarget is also the element triggering the event
    const formData = {}; // object
    for(let input of form) { // treat the form like an array
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value; // assign value to the object
        }
    }
    console.log(formData);


    var input = document.querySelector('input[type="file"]')

var data = new FormData()
data.append('the_file', input.files[0])


fetch('/files/file?path='+formData.Path, {
  method: 'POST',
  body: data
})

    // fetch('/users/' +formData.username, {
    //   method: 'PUT',
    //   headers: {
    //     "Content-Type": "application/json; charset=utf-8",
    //   },
      
    //   body: JSON.stringify(formData)
    // }).then(res => {
    //   return res.json();
    // }).then(body => {
    //   console.log(body);
    //   if (body.result == 'success') {
    //     //  vvvv HTML/DOM object given by the browser
    //     alert('Done'); // redirect the browser to other location
    //     //       ^^^ magic property that when you change it, the browser will jump to that URL
    //   } else {
    //     alert('Wrong username or password :P');
    //   }
    // });
})


document.querySelector('#delete-form') // grab the form 
.addEventListener('submit', function(event) { // add a listener upon submission
    event.preventDefault();        // ^^^ given by DOM
    // ^^^^ stop the default behaviour (submission in foreground)

    // Serialize the Form afterwards
    // const form = this; // the `this` will be converted to the element (which is form)
    const form = event.currentTarget; // event.currentTarget is also the element triggering the event
    const formData = {}; // object
    for(let input of form) { // treat the form like an array
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value; // assign value to the object
        }
    }
    console.log(formData);

    fetch('/files/directory?path='+formData.Path, {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      
      
    }).then(res => {
      return res.json();
    }).then(body => {
      console.log(body);
      if (body.result == 'success') {
        //  vvvv HTML/DOM object given by the browser
        alert('Done'); // redirect the browser to other location
        //       ^^^ magic property that when you change it, the browser will jump to that URL
      } else {
        alert('Wrong username or password :P');
      }
    });
})
};




const data = {
    files: []
  }
  
  
  
  function fileList(files){
    return `
        <ul id="file-list">
            ${
                files.map((file)=>{
                    return Files(file)
                }).join('') 
            }
        </ul>
    `
  }
  
  function deleteUser(userName){
    const user = data.users.find((user)=> user.username == userName);
    fetch('/users/' +user.username, {
      method: 'Delete',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(user)
    }).then(res => {
      return res.json();
    }).then(body => {
      console.log(body);
      if (body.result == 'success') {
        //  vvvv HTML/DOM object given by the browser
        alert('Done'); // redirect the browser to other location
        //       ^^^ magic property that when you change it, the browser will jump to that URL
      } else {
        alert('Wrong username or password :P');
      }
  
  })}
  function editUser(userName){
    const user = data.users.find((user)=> user.username == userName);
    populateForm(user);
  }
  
  function populateForm(user){
    console.log('populateForm get an object:', user);
  
    const userForm = document.querySelector('#change-form');
  
    for(let key of Object.keys(user)){
      console.log('looping key', key);
      userForm.querySelector(`[name=${key}]`).value = user[key]; // apple[key], apple.id, apple.breed, apple.weight ...
    }}
  
  function Files(file){
    return ` 
        <li class="file-item">
            <div> Directory: ${file}</div>
            
            
            
        </li>
        `
  
  }