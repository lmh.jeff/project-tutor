import * as fs from 'fs';
import * as util from 'util';
//import { PublicExamResult } from '../model/interface';
import { User } from '../model/interface';
//import { Case } from '../model/interface';
const fsReadFilePromise = util.promisify(fs.readFile);
const fsWriteFilePromise = util.promisify(fs.writeFile);

interface Profile {
    id:number;
    username: string;
    level:string;
    university: string;
    address: string;
    subject: string;
    publicExamResult: string;
    year: string;
    addressDetail: string;
    introduction: string;
    resisterDate:string;
    tel: number;
    email: string;
}

export class UserService {

    async getAllUsers() {
        const usersFile = await fsReadFilePromise('./user.json', 'utf8');
        const users = JSON.parse(usersFile); // convert JSON string to array
        // for (let user of users) {
        //   delete user.password;
        // }

        return users;
    }

    async createOneUser(newuser: User) {

        const usersFile = await fsReadFilePromise('./user.json', 'utf8');
        const users = JSON.parse(usersFile);
        const newUser = {
            'id': users.length + 1,
            'username': newuser.username,
            'password': newuser.password,
            'level': newuser.level,
            'googleLink': newuser.googleLink,
            'university': newuser.university,
            'subject': newuser.subject,
            'publicExamResult': newuser.publicExamResult,
            'year': newuser.year,
            'address': newuser.address,
            'addressDetail': newuser.addressDetail,
            'introduction': newuser.introduction,
            'recordHistory': newuser.recordHistory,
            'resisterDate': new Date(),
            'tel': newuser.tel,
            'email': newuser.email
        };
        users.push(newUser);
        await fsWriteFilePromise('./user.json', JSON.stringify(users));
    }


    async changePassword(username: string, password: string) {
        const userFile = await fsReadFilePromise('./user.json', 'utf8');
        const users = JSON.parse(userFile);
        const user = users.find(function (user: any) {
            if (user.username === username) {
                return true;
            } else {
                return false;
            }
        });

        if (typeof user === 'undefined') {

            throw new Error('no_user');
        }
        user.password = password;

        await fsWriteFilePromise('./user.json', JSON.stringify(users));
        return;
    }

    async changeProfile(username: string, body: Profile) {
        const userFile = await fsReadFilePromise('./user.json', 'utf8');
        const users = JSON.parse(userFile);
        const user = users.find(function (user: Profile) {
            if (user.username === username) {
                return true;
            } else {
                return false;
            }
        });

        if (typeof user === 'undefined') {

            throw new Error('no_user');
        }
        user.university = body.university;
        user.address = body.address;
        user.subject = body.subject;
        user.publicExamResult = body.publicExamResult;
        user.year = body.year;
        user.addressDetail = body.addressDetail;
        user.introduction = body.introduction;
        user.tel = body.tel;
        user.email = body.email;



        await fsWriteFilePromise('./user.json', JSON.stringify(users));
        return;
    }

    async delete(username: string) {
        const userFile = await fsReadFilePromise('./user.json', 'utf8');
        const users = JSON.parse(userFile);
        const user = users.find(function (user: any) {
            if (user.username === username) {
                return true;
            } else {
                return false;
            }
        });
        if (typeof user === 'undefined') {

            throw new Error('no_user');
        }

        users.splice(users.indexOf(user), 1);
        await fsWriteFilePromise('./user.json', JSON.stringify(users));
        return;

    }

}
