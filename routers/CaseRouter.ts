import * as express from 'express';



import { CaseService } from '../services/CaseService';



export class CaseRouter {

    private caseService:CaseService;

    constructor() {
      this.caseService = new CaseService();
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const cases = await this.caseService.getAllCases();
                res.json(cases);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.caseService.createOneCase(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
        router.put('/:id', async (req, res) => {

            try {
                await this.caseService.changeStatus(req.params.id, req.body);
                
                res.json({result: 'success'});
              } catch (err) {
                res.status(404).json({ result: err.message });
              }
            
        });
        
        
        
        
        router.delete('/:id', async (req, res) => {
            try {
                await this.caseService.delete(req.params.id);
                res.json({result: 'success'});
              } catch (err) {
                  
                res.status(404).json({ result: err.message });
              }



        });
        return router;
    }
}