// document.querySelector('#contact-form') // grab the form 
//   .addEventListener('submit', function(event) { // add a listener upon submission
//       event.preventDefault();        // ^^^ given by DOM
//       // ^^^^ stop the default behaviour (submission in foreground)

//       // Serialize the Form afterwards
//       // const form = this; // the `this` will be converted to the element (which is form)
//       const form = event.currentTarget; // event.currentTarget is also the element triggering the event
//       const formData = {}; // object
//       for(let input of form) { // treat the form like an array
//           if(!['submit','reset'].includes(input.type)){
//               formData[input.name] = input.value; // assign value to the object
//           }
//       }
//       console.log(formData);

//       fetch('/login', {
//         method: 'POST',
//         headers: {
//           "Content-Type": "application/json; charset=utf-8",
//         },
//         body: JSON.stringify(formData)
//       }).then(res => {
//         return res.json();
//       }).then(body => {
//         console.log(body);
//         if (body.result == 'ok') {
//           //  vvvv HTML/DOM object given by the browser
//           location.href = '/users.html'; // redirect the browser to other location
//           //       ^^^ magic property that when you change it, the browser will jump to that URL
//         } else {
//           alert('Wrong username or password :P');
//         }
//       });
//   })
