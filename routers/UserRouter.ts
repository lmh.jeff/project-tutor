import * as express from 'express';
import * as fs from 'fs';
import * as util from 'util';
const fsReadFilePromise = util.promisify(fs.readFile);
const fsWriteFilePromise = util.promisify(fs.writeFile);



export class UserRouter {


    router() {
        const router = express.Router();




        router.get('/', async (req, res) => {
            console.log("in")
            const userFile = await fsReadFilePromise('./user.json', 'utf8');
            const users = JSON.parse(userFile);
            for (let user of users) {
                delete user.password;
            }
            res.json(users);

        });

        router.get('/me', async (req, res) => {
            console.log("all")
            const userFile = await fsReadFilePromise('./user.json', 'utf8');
            const users = JSON.parse(userFile);
            for (let user of users) {
                delete user.password;
                if (user.username == req.user){
                    console.log(user);
                    res.json(user)
                    break;
                }  
            } 
 
            res.json("This user is not here");

        });

        router.post('/', async (req, res) => {
            try{

            const userFile = await fsReadFilePromise('./user.json', 'utf8');

            const users = JSON.parse(userFile);
            const newUser = {
                username: req.body.username,
                password: req.body.password,
                level: req.body.level
            };
            users.push(newUser);

            await fsWriteFilePromise('./user.json', JSON.stringify(users));


            res.json({ result: 'success' });
        }catch(err){
            res.status(500).json(err);
        }



        });
        router.put('/:username', async (req, res) => {

            const userFile = await fsReadFilePromise('./user.json', 'utf8');
            const users = JSON.parse(userFile);
            const user = users.find(function (user: any) {
                if (user.username === req.params.username) {
                    return true;
                } else {
                    return false;
                }
            });

            if (typeof user==='undefined'){
                res.status(404).json({ result: 'not found' });
                return;
            }
            user.password=req.params.password;
            
                await fsWriteFilePromise('./user.json', JSON.stringify(users));
                res.json({ result: 'success' });
            
        });
        
        
        
        
        router.delete('/:username', async (req, res) => {
            try{
            const userFile = await fsReadFilePromise('./user.json', 'utf8');
            const users = JSON.parse(userFile);
            const user = users.find(function (user: any) {
                if (user.username === req.params.username) {
                    return true;
                } else {
                    return false;
                }
            });
            if (typeof user==='undefined'){
                res.status(404).json({ result: 'not found' });
                return;
            }

            users.splice(users.indexOf(user),1);
            await fsWriteFilePromise('./user.json', JSON.stringify(users));
                res.json({ result: 'success' });
            }catch(err){
                res.status(500).json(err);
            }



        });
        return router;
    }
}