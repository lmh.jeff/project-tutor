import * as express from 'express';



import { UserService } from '../services/UserService';



export class UserRouter {

    private userService:UserService;

    constructor() {
      this.userService = new UserService();
    }
  
  
    router() {
        
        const router = express.Router();
        




        

            router.get('/', async (req, res) => {
                const users = await this.userService.getAllUsers();
                res.json(users);
              });
          

        
        router.post('/', async (req, res) => {
            
                try {
                  await this.userService.createOneUser(req.body);
                  res.json({result: 'success'});
                } catch (err) {
                    res.status(404).json({ result: err.message });
                }
              
          


        });
        router.put('/:username', async (req, res) => {
            console.log("here")
            try {
                await this.userService.changeProfile(req.user.username, req.body);
                
                res.json({result: 'success'});
              } catch (err) {
                res.status(404).json({ result: err.message });
              }
            
        });
        
        
        router.get('/me', async (req, res) => {
            const users = await this.userService.getAllUsers();
            let box = null;
            for (let user of users) {
                delete user.password;
                
                if (user.username == req.user.username){
                    console.log(user);
                   box = user
                    break;
                }  
            } 
 
            res.json(box);

        });
        
        
        router.delete('/:username', async (req, res) => {
            try {
                await this.userService.delete(req.params.username);
                res.json({result: 'success'});
              } catch (err) {
                  
                res.status(404).json({ result: err.message });
              }



        });
        return router;
    }
}