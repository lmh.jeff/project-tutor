window.onload = function() {
    populateDirectoriesAndFiles();
  
    const output = document.querySelector('#output');
    
    output.addEventListener('dragenter', uploadDragEnter);
    output.addEventListener('dragover', uploadDragOver);
    output.addEventListener('drop', uploadDrop);
  }
  
  function uploadDragEnter(event) {
    event.preventDefault();
  }
  
  function uploadDragOver(event) {
    event.preventDefault();
  }
  
  function uploadDrop(event) {
    event.preventDefault();
    const files = event.dataTransfer.files;
    console.log(files);
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const formData = new FormData()
    
      formData.append('the_file', file)
      
      fetch('/files/file?path=' + data.currentDirectory + '/' + file.name, {
        method: 'POST',
        body: formData
      })
      .then(res => res.json())
      .then(body => {
        if (body.result === 'success') {
          populateDirectoriesAndFiles();
        } else {
          alert('something goes wrong');
        }
      });
    }
  }
  
  const data = {
    directories: [],
    files: [],
    currentDirectory: '/'
  }
  
  async function populateDirectoriesAndFiles() {
    const directoriesRes = await fetch('/files/directories?folder=' + data.currentDirectory);
    const directories = await directoriesRes.json();
  
    const filesRes = await fetch('/files/files?folder=' + data.currentDirectory);
    const files = await filesRes.json();
  
    data.directories = directories;
    data.files = files;
  
    document.querySelector('#list-directories').innerHTML = DirectoryList(directories, files);
    
    //    vvvvvv an array (HTMLCollection... just like Array)
    const directoryLinks = document.querySelector('#list-directories').querySelectorAll('a[data-directory]');
    for (let i = 0; i < directoryLinks.length; i++) {
      directoryLinks[i].addEventListener('click', changeDirectory);
    }
  }
  
  function DirectoryList(directories, files) {
  
    //                    v length
    // 'abcdef'.substr(2, 3); // cde
    // 'abcdef'.substring(2, 3); // c
    //                       ^ end index (exclusive)
  
    const lastPath = data.currentDirectory.substr(0, data.currentDirectory.lastIndexOf('/'));
    let lastPathName = lastPath.substr(lastPath.lastIndexOf('/') + 1);
    if (lastPathName === '') {
      lastPathName = '/';
    }
  
    return `
    <h1>${data.currentDirectory.substr(data.currentDirectory.lastIndexOf('/') + 1)}</h1>
    <ul>
    <i class="fas fa-folder"></i> <a href="#" data-directory="..">${lastPathName}</a>
    ${directories.map(DirectoryItem).join('')}
    ${files.map(FileItem).join('')}
    </ul>
    `
  }
  
  function DirectoryItem(directory) {
    return `
    <li>
      <i class="fas fa-folder"></i> <a href="#" data-directory="${directory}">${directory}</a>
    </li>
    `
  }
  
  function changeDirectory(event) {
    event.preventDefault();
    const directory = event.currentTarget.dataset.directory;
    if (directory === '..') {
      data.currentDirectory = data.currentDirectory.substr(0, data.currentDirectory.lastIndexOf('/'));
    } else {
      data.currentDirectory = data.currentDirectory + '/' + directory;
    }
    populateDirectoriesAndFiles();
  }
  
  function FileItem(file) {
    return `
    <li>
      <i class="fas fa-file"></i> <a href="/files/file?path=${data.currentDirectory + '/' + file}">${file}</a>
    </li>
    `
  }
  